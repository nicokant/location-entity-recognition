db.getCollection("book_tokens").aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$sort: {
				document: 1,
				sentence: 1,
				position: 1,
			}
		},

		// Stage 2
		{
			$lookup: {
			    from: "places",
			    localField: "lemma",
			    foreignField: "lemma",
			    as: "places"
			}
		},

		// Stage 3
		{
			$addFields: {
			    "places_count": {$size: "$places"},
			
			    "idx_end": {$sum: ["$idx", {$strLenCP: "$text"}]}
			}
		},

		// Stage 4
		{
			$addFields: {
				"is_place": { $gt: ['$places_count', 0]},
			}
		},

		// Stage 5
		{
			$project: {
			    document: 1,
			    sentence: 1,
			    position: 1,
			    text: 1,
			    is_place: 1,
			    idx: 1,
			    idx_end: 1,
			}
		},

		// Stage 6
		{
			$group: {
			    _id: { doc: "$document", sentence: "$sentence" },
			    doc: {$first: "$document"},
			    sentence: {$first: "$sentence"},
			    tokens: {$push: '$$ROOT'},
			    text: {$push: '$text'},
			}
		},

		// Stage 7
		{
			$addFields: {
			    "text": {
					$reduce: {
			       		input: "$text",
			       		initialValue: "",
			       		in: { $concat : ["$$value", " ", "$$this"] }
			    	}
				},
			    "entities": {
			        $map: {
			        	input: {
			        	    $filter: { 
			            		input: "$tokens", 
			            		as: 't',
			            		cond: {$eq: ['$$t.is_place', true]}
			        	    }
			        	}, 
			        	as: 't', 
			        	in: { 0: "$$t.idx", 1: "$$t.idx_end", 2: "PLC"}
			        }
			    }
			}
		},

		// Stage 8
		{
			$addFields: {
				entities_count: {$size: "$entities"},
			}
		},

		// Stage 9
		{
			$match: {
				entities_count: {$gt: 0}
			}
		},

		// Stage 10
		{
			$project: {
			    text: 1,
			    entities: 1,
			    _id: 0,
			}
		},

		// Stage 11
		{
			$out: "training_set"
		},
	],

	// Options
	{
		allowDiskUse: true
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
