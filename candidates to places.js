db.getCollection("candidates").aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
				is_place: true
			}
		},

		// Stage 2
		{
			$project: {
				lemma: 1,
			}
		},

		// Stage 3
		{
			$out: "places"
		},

	]

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
