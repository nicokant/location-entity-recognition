// Stages that have been excluded from the aggregation pipeline query
__3tsoftwarelabs_disabled_aggregation_stages = [

	{
		// Stage 5 - excluded
		stage: 5,  source: {
			$out: "verbs"
		}
	},
]

db.getCollection("book_tokens").aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
				'pos': 'VERB',
			    "text": {"$regex": "^[abcdefghijklmnopqrstuvwxyz]", "$options": "i"},	
			}
		},

		// Stage 2
		{
			$group: {
				_id: '$lemma',
				count: {$sum: 1},
				lemma: {$first: '$lemma'}
			}
		},

		// Stage 3
		{
			$match: {
				count: {$gt: 100}	
			}
		},

		// Stage 4
		{
			$sort: {
				count: -1
			}
		},

	]

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
