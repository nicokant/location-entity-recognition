db.getCollection("book_tokens").aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
			    "pos": "NOUN",
			    "text": {"$regex": "^[abcdefghijklmnopqrstuvwxyz]", "$options": "i"},
			    "children": {$gt: [{ $size: "$children" }, 0]},
			}
		},

		// Stage 2
		{
			$lookup: {
			    from: "book_tokens",
			    localField: "head",
			    foreignField: "_id",
			    as: "head"
			}
		},

		// Stage 3
		{
			$unwind: {
			    path : "$head",
			    preserveNullAndEmptyArrays : false 
			}
		},

		// Stage 4
		{
			$lookup: {
			    from: "book_tokens",
			    localField: "children",
			    foreignField: "_id",
			    as: "children"
			}
		},

		// Stage 5
		{
			$unwind: {
			    path : "$children",
			    preserveNullAndEmptyArrays : false
			}
		},

		// Stage 6
		{
			$lookup: {
			    from: "expr_tokens",
			    localField: "head.stem",
			    foreignField: "stem",
			    as: "head_expr"
			}
		},

		// Stage 7
		{
			$addFields: {
			        "head_expr": {$gt: [{ $size: "$head_expr" }, 0]},
			        "children_prep": {"$in": ["$children.pos", ['ADP', 'DET']]}, 
			}
		},

		// Stage 8
		{
			$group: {
			    '_id': '$_id',    
			    'oid': {'$first': '$_id'},
			    'lemma': { '$first': '$lemma' },
			    'lower': { '$first': '$lower' },
			    'stem': { '$first': '$stem' },
			    'pos': { '$first': '$pos' },
			    'head': { '$first': '$head' },
			    'head_expr': {$push: '$head_expr'},
			    'child_prep': {$push: '$child_prep'},
			    'children': {'$push': '$children'}
			}
		},

		// Stage 9
		{
			$addFields: {
			    //"head_verb": {$gt: [{ $size: "$head_verb" }, 0]},
			    "head_expr": {$anyElementTrue: ["$head_expr"]},
			    "child_prep": {$anyElementTrue: ["$child_prep"]},
			}
		},

		// Stage 10
		{
			$match: { 'head_expr': true, 'head.pos': 'NOUN' }
		},

		// Stage 11
		{
			$project: {
			    "oid": 1,
			    "lower": 1,
			    "lemma": 1,
			    "stem": 1,
			}
		},

		// Stage 12
		{
			$out: "candidates"
		},
	],

	// Options
	{
		allowDiskUse: true
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
