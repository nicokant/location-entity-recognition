// Stages that have been excluded from the aggregation pipeline query
__3tsoftwarelabs_disabled_aggregation_stages = [

	{
		// Stage 12 - excluded
		stage: 12,  source: {
			$project: {
			    "oid": 1,
			    "lower": 1,
			    "lemma": 1,
			    "stem": 1,
			    "is_place": 1,
			}
		}
	},

	{
		// Stage 14 - excluded
		stage: 14,  source: {
			$sort: {
			    "counter": -1
			}
		}
	},
]

db.getCollection("book_tokens").aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
			    "pos": "PROPN",
			    "text": {"$regex": "^[abcdefghijklmnopqrstuvwxyz]", "$options": "i"},
			    "children": {$gt: [{ $size: "$children" }, 0]},
			}
		},

		// Stage 2
		{
			$lookup: {
			    from: "book_tokens",
			    localField: "head",
			    foreignField: "_id",
			    as: "head"
			}
		},

		// Stage 3
		{
			$unwind: {
			    path : "$head",
			    preserveNullAndEmptyArrays : false 
			}
		},

		// Stage 4
		{
			$lookup: {
			    from: "book_tokens",
			    localField: "children",
			    foreignField: "_id",
			    as: "children"
			}
		},

		// Stage 5
		{
			$unwind: {
			    path : "$children",
			    preserveNullAndEmptyArrays : false
			}
		},

		// Stage 6
		{
			$lookup: {
			    from: "preps_tokens",
			    localField: "children.stem",
			    foreignField: "stem",
			    as: "child_prep"
			}
		},

		// Stage 7
		{
			$addFields: {
			        "child_prep": {$gt: [{ $size: "$child_prep" }, 0]},
			}
		},

		// Stage 8
		{
			$group: {
			    '_id': '$_id',
			    'oid': {'$first': '$_id'},
			    'lemma': { '$first': '$lemma' },
			    'lower': { '$first': '$lower' },
			    'stem': { '$first': '$stem' },
			    'pos': { '$first': '$pos' },
			    'head': { '$first': '$head' },
			    'child_prep': {$push: '$child_prep'},
			    'children': {'$push': '$children'}
			}
		},

		// Stage 9
		{
			$lookup: {
			    from: "verb_tokens",
			    localField: "head.stem",
			    foreignField: "stem",
			    as: "head_verb"
			}
		},

		// Stage 10
		{
			$addFields: {
			    "head_verb": {$gt: [{ $size: "$head_verb" }, 0]},
			    "child_prep": {$anyElementTrue: ["$child_prep"]},
			    "is_place": false,
			}
		},

		// Stage 11
		{
			$match: { 'child_prep': true, 'head_verb': true }
		},

		// Stage 13
		{
			$group: {
			    "_id": "$lemma",
			    "lemma": {$first: "$lemma"},
			    "is_place": {$first: "$is_place"},
			    "counter": {$sum: 1}
			}
		},

		// Stage 15
		{
			$out: "candidates_propn"
		},
	],

	// Options
	{
		allowDiskUse: true
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
