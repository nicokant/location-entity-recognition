// Stages that have been excluded from the aggregation pipeline query
__3tsoftwarelabs_disabled_aggregation_stages = [

	{
		// Stage 14 - excluded
		stage: 14,  source: {
			$group: {
			    "_id": "$lemma",
			    "lemma": {$first: "$lemma"},
			    "count": { $sum: 1 },
			}
		}
	},

	{
		// Stage 15 - excluded
		stage: 15,  source: {
			$sort: {
			    'count': -1
			}
		}
	},
]

db.getCollection("book_tokens").aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$match: {
			    "pos": "NOUN",
			    "text": {"$regex": "^[abcdefghijklmnopqrstuvwxyz]", "$options": "i"},
			    "children": {$gt: [{ $size: "$children" }, 0]},
			}
		},

		// Stage 2
		{
			$lookup: {
			    from: "book_tokens",
			    localField: "head",
			    foreignField: "_id",
			    as: "head"
			}
		},

		// Stage 3
		{
			$unwind: {
			    path : "$head",
			    preserveNullAndEmptyArrays : false 
			}
		},

		// Stage 4
		{
			$lookup: {
			    from: "book_tokens",
			    localField: "children",
			    foreignField: "_id",
			    as: "children"
			}
		},

		// Stage 5
		{
			$unwind: {
			    path : "$children",
			    preserveNullAndEmptyArrays : false
			}
		},

		// Stage 6
		{
			$lookup: {
			    from: "preps_tokens",
			    localField: "children.stem",
			    foreignField: "stem",
			    as: "child_prep"
			}
		},

		// Stage 7
		{
			$addFields: {
			        "child_prep": {$gt: [{ $size: "$child_prep" }, 0]},
			}
		},

		// Stage 8
		{
			$project: {
			    "lower": 1,
			    "lemma": 1,
			    "pos": 1,
			    "stem": 1,
			    "child_prep": 1,
			    "head": {
			        "lower": 1,
			        "stem": 1,
			        "pos": 1,
			        "lemma": 1,
			    },
			    "children": {
			        "lower": 1,
			        "stem": 1,
			        "pos": 1,
			        "lemma": 1,
			//        "head": {
			//            "lower": 1,
			//            "stem": 1,
			//            "pos": 1,
			//            "lemma": 1,
			//        },
			//        "children": {
			//            "lower": 1,
			//            "stem": 1,
			//            "pos": 1,
			//            "lemma": 1,
			//        }
			    }
			}
		},

		// Stage 9
		{
			$group: {
			    '_id': '$_id',
			    'lemma': { '$first': '$lemma' },
			    'lower': { '$first': '$lower' },
			    'stem': { '$first': '$stem' },
			    'pos': { '$first': '$pos' },
			    'head': { '$first': '$head' },
			    'child_prep': {$push: '$child_prep'},
			    'children': {'$push': '$children'}
			}
		},

		// Stage 10
		{
			$lookup: {
			    from: "verb_tokens",
			    localField: "head.stem",
			    foreignField: "stem",
			    as: "head_verb"
			}
		},

		// Stage 11
		{
			$addFields: {
			    "head_verb": {$gt: [{ $size: "$head_verb" }, 0]},
			    "child_prep": {$anyElementTrue: ["$child_prep"]},
			}
		},

		// Stage 12
		{
			$match: { 'child_prep': true, 'head_verb': true }
		},

		// Stage 13
		{
			$project: {
			    "lower": 1,
			    "lemma": 1,
			    "stem": 1,
			}
		},

		// Stage 16
		{
			$out: "candidates"
		},
	],

	// Options
	{
		allowDiskUse: true
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
